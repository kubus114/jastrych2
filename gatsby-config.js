module.exports = {
  pathPrefix: `/new/public`,
  siteMetadata: {
    title: `PROFESJONALNE WYLEWKI`,
    description: `Wykonujemy posadzki przemysłowe, techniczne oraz ozdobne, sprawdź co z naszej oferty będzie zgodne z oczekiwaniami i przeznaczeniem posadzki.`,
    author: `@Jakub Awieruk @Hubert Hałun`
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        path: `./src/images`,
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/logos/logo.png`, // This path is relative to the root of the site.
      }
    },
    `gatsby-plugin-styled-components`
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
