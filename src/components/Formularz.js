import React from "react";
import Axios from "axios";
import {Form} from 'react-bootstrap'
import {useFormik} from "formik";
import styled from 'styled-components'

function SignupForm({imie, nazwisko, mail, telefon, wiadomosc, zgoda}){
  const formik = useFormik({
    initialValues: {
      imie:'',
      nazwisko:'',
      email: '',
      telefon:'',
      rodo:'brak zgody',
      wiadomosc:''
    },
    onSubmit:values => {
      Axios({
        method: "post",
        url: "https://getform.io/f/a6cd5ec1-7d88-456a-b16c-06acc57729f5",
        data: values
      })
    },
  });
   
  return (
    <StyledForm onSubmit={formik.handleSubmit} encType="multipart/form-data">
      {imie?
      <>
      <StyledInput
         id="imie"
         name="imie"
         type="text"
         placeholder="Imię"
         required
         onChange={formik.handleChange}
         value={formik.values.imie}
      /><br/>
      </>:<></>
      }
      {nazwisko?
      <>
      <StyledInput
        id="nazwisko"
        name="nazwisko"
        type="text"
        placeholder="Nazwisko"
        required
        onChange={formik.handleChange}
        value={formik.values.nazwisko}
      /><br/>
      </>:<></>
      }
      {mail?
      <>
      <StyledInput
        id="email"
        name="email"
        type="email"
        placeholder="Adres e-mail"
        required
        onChange={formik.handleChange}
        value={formik.values.email}
      /><br/>
      </>:<></>
      }
      {telefon?
      <>
      <StyledInput
        id="telefon"
        name="telefon"
        type="tel"
        placeholder="numer telefonu"
        onChange={formik.handleChange}
        value={formik.values.telefon}
      /><br/>
      </>:<></>
      }
    {zgoda?
     <> 
      
      <StyledInput
        id="rodo"
        name="rodo"
        type="checkbox"
        required
        value="zgoda wyrażona"
        onChange={formik.handleChange}
        className="w-auto p--5"
      /> 
      <p>
        Wyrażam zgodę na przetwarzanie moich danych osobowych przez Jastrych s.c. w celu 
        prowadzenia rekrutacji.
      </p>
    </>:<></>
    }
    {wiadomosc?
    <>
    <StyledInput as="textarea"
      id="wiadomosc"
      name="wiadomosc"
      placeholder="Twoja wiadomość"
      required
      onChange={formik.handleChange}
      value={formik.values.wiadomosc}
    /><br/></>:<></>
        }
      
       <StyledButton id="submit" type="submit">Wyślij</StyledButton>
     </StyledForm>
   );
 };

 export default SignupForm

 const StyledButton = styled.button`
  color: rgb(0,0,0);
  background-color: var(--orange);
  width:25%;
  border-radius:10px 0 10px 0;
  min-width: 100px;
  height: 35px;
  font-weight:700;
  border:none;
  box-shadow:2px 5px 5px 0 rgba(0,0,0,0.4);
  :hover{
    background-color:rgba(255,164,23,0.9);
  }
 `

 const StyledForm = styled(Form)`
  text-align:center;
 `

 const StyledInput = styled.input`
  width:50%;
  min-width: 250px;
  margin: 0 0 9px 0;
  border: 2px solid rgb(72,72,72);
  border-radius:4px;
  background-color: rgb(60,60,60);
  color: #fbfbfb;
  padding:5px 5px;

  ::placeholder{
    color: #fbfbfb;

  }

 `