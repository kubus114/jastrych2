const { hot } = require("react-hot-loader/root")

// prefer default export if available
const preferDefault = m => (m && m.default) || m


exports.components = {
  "component---src-pages-404-js": hot(preferDefault(require("C:\\Dev\\jastrych2\\src\\pages\\404.js"))),
  "component---src-pages-career-js": hot(preferDefault(require("C:\\Dev\\jastrych2\\src\\pages\\career.js"))),
  "component---src-pages-contact-js": hot(preferDefault(require("C:\\Dev\\jastrych2\\src\\pages\\contact.js"))),
  "component---src-pages-dom-js": hot(preferDefault(require("C:\\Dev\\jastrych2\\src\\pages\\dom.js"))),
  "component---src-pages-gallery-js": hot(preferDefault(require("C:\\Dev\\jastrych2\\src\\pages\\gallery.js"))),
  "component---src-pages-hale-js": hot(preferDefault(require("C:\\Dev\\jastrych2\\src\\pages\\hale.js"))),
  "component---src-pages-index-js": hot(preferDefault(require("C:\\Dev\\jastrych2\\src\\pages\\index.js"))),
  "component---src-pages-zewnetrzne-js": hot(preferDefault(require("C:\\Dev\\jastrych2\\src\\pages\\zewnetrzne.js")))
}

